<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Dashboard extends Controller
{
    //
    public function getDashboardPage() {
        return view('dashboard');
    }
    public function getSample1() {
        return view('template/sample-1');
    }
}
