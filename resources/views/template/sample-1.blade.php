<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>FlOW_MEDIA</title>

  <!-- Bootstrap Core CSS -->
  <link href="{{URL('/assets/css/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">

  <!-- Custom Fonts -->
  <link href="{{URL('/assets/css/fontawesome-free/css/all.min.css')}}" rel="stylesheet" type="text/css">
  <link href="{{URL('/assets/css/simple-line-icons/css/simple-line-icons.css')}}" rel="stylesheet">

  <!-- Custom CSS -->
  <link href="{{URL('/assets/css/stylish-portfolio.min.css')}}" rel="stylesheet">

</head>

<body id="page-top">

  <!-- Header -->
    <header class="p-1 container text-center my-auto text-grey bg-white">
        
          <h5 class="mt-2">
          السَّلاَمُ عَلَيْكُمْ وَرَحْمَةُ اللهِ وَبَرَكَاتُهُ
          </h5>
    </header>

  <!-- About -->
  <section class="mb-5 bg-white mb-4" id="about">
    <div class="container text-center">
      <div class="row">
        <div class="col-lg-10 mx-auto">
          <img src="{{URL('/assets/img/1.jpg')}}" class="img-fluid mb-2 mx-auto">
          <hr>
          <h4>
       بِسْــــــــــــــــــمِ اللهِ الرَّحْمَنِ الرَّحِيْمِ
          </h4>
          <p class="lead mb-1">
            Mahasuci Allah SWT,<br>
            yang telah menciptakan makhluk-nya berpasang-pasangan<br>
            Ya Allah, rahmatilah pernikahan putra putri kami<br>
          </p>
          <a class="btn btn-dark btn-xl js-scroll-trigger" href="#OrangTua">Yang Bernama</a>
        </div>
      </div>
    </div>
  </section>

  <!-- OrangTua -->
  <section class="content-section bg-info text-white text-center" id="OrangTua">
    <div class="container text-center">

      <div class="row">
        <div class="media p-3 col-12 mb-5 mb-lg-0" width="100%" height="100%">
          <span class="service-icon rounded-circle mx-auto mb-3">
            <i class="icon-he"></i>
          </span>
          <div class="media-body m-2">
            <h4>Bangun <small><i>..........</i></small></h4>
            <p>Putra Pertama Dari BPK, Tohirin & Ibu Rukini Umi Latifah</p>
          </div>
        </div>

        <div class="media p-3 col-12 mb-5 mb-lg-0" width="100%" height="100%">
          <div class="media-body m-2">
            <h4>Dini <small><i>..............</i></small></h4>
            <p>Putri Kedua Dari BPK, H.Zaenal Abidin, S.Pd & Ibu Hj.Latifah</p>
          </div>
          <span class="service-icon rounded-circle mx-auto mb-3">
            <i class="icon-he"></i>
          </span>
        </div>
        <a class="p-3 col-12 mb-5 mb-lg-0 btn btn-dark btn-xl js-scroll-trigger" href="#WaktuTempat">Yang akan dilaksanakan pada</a>
      </div>
    </div>
  </section>

 <!-- Tempat -->
  <section class="content-section bg-secondary mt-6 text-white text-center" id="WaktuTempat">
    <div class="container">

      <div class="row text-center">
        <div class="media p-3 col-12 mb-5 mb-lg-0" width="100%" height="100%">
          <span class="service-icon rounded-circle mx-auto mb-3">
            <i class="icon-clock"></i>
          </span>
          <div class="media-body m-2">
            <h4>Akad Nikah : <small><i>Sabtu, 09 Maret 2019 | 09:00 WIB</i></small></h4>
          </div>
        </div>

        <div class="media p-3 col-12 mb-5 mb-lg-0" width="100%" height="100%">
          <div class="media-body m-2">
            <h4>Resepsi : <small><i>Sabtu, 09 Maret 2019 | 13:00 WIB </i></small></h4>
          </div>
          <span class="service-icon rounded-circle mx-auto mb-3">
            <i class="icon-people"></i>
          </span>
        </div>
        <a class="p-3 col-12 mb-5 mb-lg-0 btn btn-dark btn-xl js-scroll-trigger" href="#Tempat">Yang Bertempat Di</a>
      </div>
    </div>
  </section>

  <!-- Portfolio -->
  <section class="content-section" id="Tempat">
    <div class="container">
      <div class="content-section-heading text-center">
        <h3 class="text-secondary mb-0">Tempat Pelaksanaan</h3>
        <h2 class="mb-5">Tempat Resepsi</h2>
      </div>

      <div class="row no-gutters">
        <div class="col-12">
          <a class="portfolio-item" href="#">
            <span class="caption">
              <span class="caption-content">
                <h2>Kopi Daong</h2>
                <p class="mb-0 text-grey">
                Pancawati, Caringin, Bogor, West Java<br>
                Pancawati, Kec. Caringin, Bogor, Jawa Barat<br>
                0812-1137-1702<br>
              </p>
              </span>
            </span>
            <img class="img-fluid" src="{{URL('/assets/img/5.jpg')}}" alt="">
          </a>
        </div>

    </div>
  </section>

  <!-- Map -->
  <section id="contact" class="map">
    <iframe width="100%" height="100%" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3962.368393730612!2d106.87340001427583!3d-6.724824795138417!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69cbed8dc4caeb%3A0x29e27eabe04db298!2sKopi%20Daong!5e0!3m2!1sid!2sid!4v1570936087451!5m2!1sid!2sid"></iframe>
    <br />

  </section>

  <!-- Footer -->
  <footer class="footer text-center">
    <div class="container">
      <ul class="list-inline mb-5">
        <li class="list-inline-item">
          <a class="social-link rounded-circle text-white mr-3" href="#">
            <i class="icon-social-facebook"></i>
          </a>
        </li>
        <li class="list-inline-item">
          <a class="social-link rounded-circle text-white mr-3" href="#">
            <i class="icon-social-instagram"></i>
          </a>
        </li>
        <li class="list-inline-item">
          <a class="social-link rounded-circle text-white" href="#">
            <i class="icon-social-twitter"></i>
          </a>
        </li>
      </ul>
      <p class="text-muted small mb-0">Copyright &copy; Your Website 2019</p>
    </div>
  </footer>

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded js-scroll-trigger" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Bootstrap core JavaScript -->
  <script src="{{URL('/assets/css/jquery/jquery.min.js')}}"></script>
  <script src="{{URL('/assets/css/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

  <!-- Plugin JavaScript -->
  <script src="{{URL('/assets/css/jquery-easing/jquery.easing.min.js')}}"></script>

  <!-- Custom scripts for this template -->
  <script src="{{URL('/assets/js/stylish-portfolio.min.js')}}"></script>

</body>

</html>